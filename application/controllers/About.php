<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']	= 'About Us';
		$data['page']	= 'about';
		$data['about']			= $this->admin_model->get_section('about');
		$data['settings']		= $this->admin_model->get_settings();
		$data['meta']			= $this->admin_model->get_metatags();
		$data['slider_images']	= $this->admin_model->get_slider_images();
		$data['services']		= $this->admin_model->get_services();
		$data['testimonials']	= $this->admin_model->get_hp_testimonials();
		$data['active']	= 2;
		$this->load->view('page',$data);
	}
}
