<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['active'] = 0;
		$data['title']	= 'Administration';
		$data['logged']	= false;
		if($this->session->userdata('username')){
			$data['page']	= 'main';
			$data['logged']	= true;
		}
		$this->load->view('admin/index',$data);
		
	}
	
	public function login(){
		if($this->admin_model->login()){
			$this->session->set_flashdata('success','Welcome!');
		}else{
			$this->session->set_flashdata('error','Incorrect credentials! Please try again.');
		}
		redirect(site_url('admin'),'location');
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url('admin'),'location');
	}

	public function change_password(){
		if($this->session->userdata('username')){
			if(isset($_POST['new_pass'])){
				if($this->admin_model->change_password()){
					$this->session->set_flashdata('success','Password changed successfully.');
					redirect(site_url('admin'),'location');
				}else{
					$this->session->set_flashdata('error','Old password is incorrect. Please try again.');
					redirect(site_url('admin/change_password'),'location');
				}
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 0;
				$data['title']		= 'Administration';
				$data['page']		= 'change_password';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function slider_images(){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 1;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_slider_images();
			$data['page']	= 'slider_images';
			
			if (isset($_POST['submit'])) {
    			$j = 0; 
				$target_path = "uploads/slider/"; 
    			for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
        			$validextensions = array("jpeg", "jpg", "png");  
        			$ext = explode('.', basename($_FILES['file']['name'][$i])); 
        			$file_extension = strtolower(end($ext)); 
        
					$filename = md5(uniqid()) . "." . $ext[count($ext) - 1];
					
        			$j = $j + 1;
	  				if (($_FILES["file"]["size"][$i] < 1000000) && in_array($file_extension, $validextensions)) {
            			if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path.$filename)) {
            				$this->admin_model->add_image($filename);
            			} else {
                			$this->session->set_flashdata('error','Upload failed;');;
            			}
        			} else {
            			$this->session->set_flashdata('error','***Invalid file Size or Type***');
        			}
    			}
				$this->session->set_flashdata('success','Images added succesfully');
				redirect(site_url('admin/slider_images'),'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function order(){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 1;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_slider_images();
			$data['page']	= 'order_slider_images';
			
			if (isset($_POST['id'])) {
    			$this->admin_model->order_images();
            	redirect(site_url('admin/slider_images'),'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_image($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_image($id);
            $this->session->set_flashdata('success','Images removed succesfully');
			redirect(site_url('admin/slider_images'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function home(){
		if($this->session->userdata('username')){
			if(isset($_POST['content'])){
				$this->admin_model->edit_section('home');
				$this->session->set_flashdata('success','Changes saved successfully.');
				redirect(site_url('admin/home'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 2;
				$data['title']		= 'Administration';
				$data['about']	= $this->admin_model->get_section('home');
				$data['page']		= 'home';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function about(){
		if($this->session->userdata('username')){
			if(isset($_POST['content'])){
				$this->admin_model->edit_section('about');
				$this->session->set_flashdata('success','Changes saved successfully.');
				redirect(site_url('admin/about'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 3;
				$data['title']		= 'Administration';
				$data['about']	= $this->admin_model->get_section('about');
				$data['page']		= 'about';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function services(){
		if($this->session->userdata('username')){
			$data['logged']		= true;	
			$data['active'] 	= 4;
			$data['title']		= 'Administration';
			$data['services']	= $this->admin_model->get_services();
			$data['page']		= 'services';
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function add_service(){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$this->admin_model->add_service();
				$this->session->set_flashdata('success','New service added successfully!');
				redirect(site_url('admin/services'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 4;
				$data['title']		= 'Administration';
				$data['form']		= 'Add';
				$data['action']		= site_url('admin/add_service');
				$data['button']		= 'Add';
				$data['page']		= 'services_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function edit_service($id){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$this->admin_model->edit_service($id);
				$this->session->set_flashdata('success','Changes saved successfully!');
				redirect(site_url('admin/services'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 4;
				$data['title']		= 'Administration';
				$data['form']		= 'Edit';
				$data['action']		= site_url('admin/edit_service').'/'.$id;
				$data['button']		= 'Save Changes';
				$data['service']	= $this->admin_model->get_service($id);
				$data['page']		= 'services_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_service($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_service($id);
			$this->session->set_flashdata('success','Service removed successfully!');	
			redirect(site_url('admin/services'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function service_images($id){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 4;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_service_images($id);
			$data['service']= $this->admin_model->get_service($id);
			$data['page']	= 'service_images';
			
			if (isset($_POST['submit'])) {
    			$j = 0; 
				$target_path = "uploads/services/"; 
    			for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
        			$validextensions = array("jpeg", "jpg", "png");  
        			$ext = explode('.', basename($_FILES['file']['name'][$i])); 
        			$file_extension = strtolower(end($ext)); 
        
					$filename = md5(uniqid()) . "." . $ext[count($ext) - 1];
					
        			$j = $j + 1;
	  				if (($_FILES["file"]["size"][$i] < 1000000) && in_array($file_extension, $validextensions)) {
            			if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path.$filename)) {
            				$this->admin_model->add_service_image($id,$filename);
            			} else {
                			$this->session->set_flashdata('error','Upload failed;');;
            			}
        			} else {
            			$this->session->set_flashdata('error','***Invalid file Size or Type***');
        			}
    			}
				$this->session->set_flashdata('success','Images added succesfully');
				redirect(site_url('admin/service_images').'/'.$id,'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function order_service_images($id){
		if($this->session->userdata('username')){
			$data['id']		= $id;
			$data['logged']	= true;	
			$data['active'] = 4;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_service_images($id);
			$data['service']= $this->admin_model->get_service($id);
			$data['page']	= 'order_service_images';
			
			if (isset($_POST['id'])) {
    			$this->admin_model->order_service_images($id);
            	redirect(site_url('admin/service_images').'/'.$id,'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_service_image($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_service_image($id);
            $this->session->set_flashdata('success','Images removed succesfully');
			redirect($_SERVER['HTTP_REFERER'],'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function testimonials(){
		if($this->session->userdata('username')){
			$data['logged']		= true;	
			$data['active'] 	= 5;
			$data['title']		= 'Administration';
			$data['testimonials']	= $this->admin_model->get_testimonials();
			$data['page']		= 'testimonials';
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function order_testimonials(){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 5;
			$data['title']	= 'Administration';
			$data['testimonials']	= $this->admin_model->get_testimonials();
			$data['page']	= 'order_testimonials';
			
			if (isset($_POST['id'])) {
    			$this->admin_model->order_testimonials();
            	redirect(site_url('admin/testimonials'),'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function add_testimonial(){
		if($this->session->userdata('username')){
			if(isset($_POST['people'])){
				$this->admin_model->add_testimonial();
				$this->session->set_flashdata('success','New testimonial added successfully!');
				redirect(site_url('admin/testimonials'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 5;
				$data['title']		= 'Administration';
				$data['form']		= 'Add';
				$data['action']		= site_url('admin/add_testimonial');
				$data['button']		= 'Add';
				$data['page']		= 'testimonials_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function edit_testimonial($id){
		if($this->session->userdata('username')){
			if(isset($_POST['people'])){
				$this->admin_model->edit_testimonial($id);
				$this->session->set_flashdata('success','Changes saved successfully!');
				redirect(site_url('admin/testimonials'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 5;
				$data['title']		= 'Administration';
				$data['form']		= 'Edit';
				$data['action']		= site_url('admin/edit_testimonial').'/'.$id;
				$data['button']		= 'Save Changes';
				$data['testimonial']= $this->admin_model->get_testimonial($id);
				$data['page']		= 'testimonials_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_testimonial($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_testimonial($id);
			$this->session->set_flashdata('success','Testimonial removed successfully!');	
			redirect(site_url('admin/testimonials'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function galleries(){
		if($this->session->userdata('username')){
			$data['logged']		= true;	
			$data['active'] 	= 6;
			$data['title']		= 'Administration';
			$data['galleries']	= $this->admin_model->get_galleries();
			$data['page']		= 'galleries';
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function add_gallery(){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$id = $this->admin_model->add_gallery();
				$this->session->set_flashdata('success','New gallery added successfully!');
				if($_POST['type'] == 'image'){
					redirect(site_url('admin/gallery_images').'/'.$id,'location');
				}else{
					redirect(site_url('admin/galleries'),'location');	
				}
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 6;
				$data['title']		= 'Administration';
				$data['form']		= 'Add';
				$data['action']		= site_url('admin/add_gallery');
				$data['button']		= 'Add';
				$data['page']		= 'gallery_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function edit_gallery($id){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$this->admin_model->edit_gallery($id);
				$this->session->set_flashdata('success','Changes saved successfully!');
				redirect(site_url('admin/galleries'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 6;
				$data['title']		= 'Administration';
				$data['form']		= 'Edit';
				$data['action']		= site_url('admin/edit_gallery').'/'.$id;
				$data['button']		= 'Save Changes';
				$data['gallery']	= $this->admin_model->get_gallery($id);
				$data['page']		= 'gallery_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_gallery($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_gallery($id);
			$this->session->set_flashdata('success','Gallery removed successfully!');	
			redirect(site_url('admin/galleries'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
	public function gallery_images($id){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 6;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_gallery_images($id);
			$data['id']		= $id;
			$data['page']	= 'gallery_images';
			
			if (isset($_POST['submit'])) {
    			$j = 0; 
				$target_path = "uploads/gallery/"; 
    			for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
        			$validextensions = array("jpeg", "jpg", "png");  
        			$ext = explode('.', basename($_FILES['file']['name'][$i])); 
        			$file_extension = strtolower(end($ext)); 
        
					$filename = md5(uniqid()) . "." . $ext[count($ext) - 1];
					
        			$j = $j + 1;
	  				if (($_FILES["file"]["size"][$i] < 1000000) && in_array($file_extension, $validextensions)) {
            			if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path.$filename)) {
            				$this->admin_model->add_gallery_image($id,$filename);
            			} else {
                			$this->session->set_flashdata('error','Upload failed;');;
            			}
        			} else {
            			$this->session->set_flashdata('error','***Invalid file Size or Type***');
        			}
    			}
				$this->session->set_flashdata('success','Images added succesfully');
				redirect(site_url('admin/gallery_images').'/'.$id,'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function order_gallery_images($id){
		if($this->session->userdata('username')){
			$data['id']		= $id;
			$data['logged']	= true;	
			$data['active'] = 4;
			$data['title']	= 'Administration';
			$data['images']	= $this->admin_model->get_gallery_images($id);
			$data['gallery']= $this->admin_model->get_gallery($id);
			$data['page']	= 'order_gallery_images';
			
			if (isset($_POST['id'])) {
    			$this->admin_model->order_gallery_images($id);
            	redirect(site_url('admin/gallery_images').'/'.$id,'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_g_image($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_g_image($id);
            $this->session->set_flashdata('success','Images removed succesfully');
			redirect($_SERVER['HTTP_REFERER'],'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function meta($key=false){
		if($this->session->userdata('username')){
			if(isset($_POST['content'])){
				$this->admin_model->edit_section('meta_'.$key);
				$this->session->set_flashdata('success','Changes saved successfully.');
				redirect(site_url('admin/meta'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 7;
				$data['title']		= 'Administration';
				$data['meta']		= $this->admin_model->get_metatags();
				$data['page']		= 'meta';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	//Articles
	public function articles(){
		if($this->session->userdata('username')){
			$data['logged']		= true;	
			$data['active'] 	= 8;
			$data['title']		= 'Administration';
			$data['articles']	= $this->admin_model->get_articles();
			$data['page']		= 'articles';
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function add_article(){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$this->admin_model->add_article();
				$this->session->set_flashdata('success','New Article added successfully!');
				redirect(site_url('admin/articles'),'location');
			}else{
				$data['logged']		= true;	
				$data['active'] 	= 8;
				$data['title']		= 'Administration';
				$data['form']		= 'Add';
				$data['action']		= site_url('admin/add_article');
				$data['button']		= 'Add';
				$data['page']		= 'articles_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function edit_article($id){
		if($this->session->userdata('username')){
			if(isset($_POST['title'])){
				$this->admin_model->edit_article($id);
				$this->session->set_flashdata('success','Changes saved successfully!');
				redirect(site_url('admin/articles'),'location');
			}else{
				$data['logged']	= true;	
				$data['active'] = 8;
				$data['title']	= 'Administration';
				$data['form']	= 'Edit';
				$data['action']	= site_url('admin/edit_article').'/'.$id;
				$data['button']	= 'Save Changes';
				$data['article']= $this->admin_model->get_article($id);
				$data['page']	= 'articles_form';
				$this->load->view('admin/index',$data);
			}
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function order_articles(){
		if($this->session->userdata('username')){
			$data['logged']	= true;	
			$data['active'] = 8;
			$data['title']	= 'Administration';
			$data['articles']	= $this->admin_model->get_articles();
			$data['page']	= 'order_articles';
			
			if (isset($_POST['id'])) {
    			$this->admin_model->order_articles();
            	redirect(site_url('admin/articles'),'location');
			}
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function remove_article($id){
		if($this->session->userdata('username')){
			$this->admin_model->remove_article($id);
			$this->session->set_flashdata('success','Article removed successfully!');	
			redirect(site_url('admin/articles'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	//settings
	public function settings(){
		if($this->session->userdata('username')){
			$data['logged']		= true;	
			$data['active'] 	= 9;
			$data['title']		= 'Administration';
			$data['settings']	= $this->admin_model->get_settings_raw();
			$data['page']		= 'settings';
			$this->load->view('admin/index',$data);
		}else{
			redirect(site_url('admin'),'location');
		}
	}

	public function update_setting($id){
		if($this->session->userdata('username')){
			$this->admin_model->update_setting($id);
			$this->session->set_flashdata('success','Changes saved successfully!');
			redirect(site_url('admin/settings'),'location');
		}else{
			redirect(site_url('admin'),'location');
		}
	}
	
}
