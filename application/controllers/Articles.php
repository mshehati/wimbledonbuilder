<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index(){
		redirect('/','refresh');
	}
	
	public function a($id)
	{
		$article			= $this->admin_model->get_article($id);
		$data['title']		= $article['title'];
		$data['page']		= 'article';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']		= $this->admin_model->get_metatags();
		$data['services']	= $this->admin_model->get_services();
		$data['article']	= $article;
		$data['active']		= 0;
		$this->load->view('page',$data);
	}
}
