<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']		= 'Contact Us';
		$data['page']		= 'contact';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']		= $this->admin_model->get_metatags();
		$data['services']	= $this->admin_model->get_services();
		$data['active']		= 6;
		$this->load->view('page',$data);
	}
	
	public function send(){
		if($this->admin_model->notify()){
			$this->session->set_flashdata('contact','successfull');
		}else{
			$this->session->set_flashdata('contact-error','error');
		}
		redirect(site_url('contact'),'location');
	}
}
