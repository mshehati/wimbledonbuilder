<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']	= 'Gallery';
		$data['page']	= 'galleries';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']	= $this->admin_model->get_metatags();
		$data['galleries']		= $this->admin_model->get_galleries();
		$data['services']		= $this->admin_model->get_services();
		$data['active']	= 4;
		$this->load->view('page',$data);
	}
	
	public function view($g)
	{
		$data['title']	= 'Gallery';
		$data['page']	= 'gallery';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']	= $this->admin_model->get_metatags();
		$data['gallery']		= $this->admin_model->get_gallery_h($g);
		$data['images']			= $this->admin_model->get_gallery_images($data['gallery']['id']);
		$data['services']		= $this->admin_model->get_services();
		$data['active']	= 4;
		$this->load->view('page',$data);
	}
}
