<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']	= 'Home';
		$data['page']	= 'home';
		$data['home']	= $this->admin_model->get_section('home');
		$data['meta']	= $this->admin_model->get_metatags();
		$data['settings']		= $this->admin_model->get_settings();
		$data['slider_images']	= $this->admin_model->get_slider_images();
		$data['services']		= $this->admin_model->get_services();
		$data['articles']	= $this->admin_model->get_hp_articles();
		$data['active']	= 1;
		$this->load->view('page',$data);
	}
}
