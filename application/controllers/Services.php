<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']		= 'Services';
		$data['page']		= 'services';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']		= $this->admin_model->get_metatags();
		$data['services']	= $this->admin_model->get_services();
		$data['active']		= 3;
		$this->load->view('page',$data);
	}
	
	public function s($title)
	{
		$data['title']		= 'Services';
		$data['page']		= 'service';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']		= $this->admin_model->get_metatags();
		$data['services']	= $this->admin_model->get_services();
		$service			= $this->admin_model->get_service_t($title);
		$data['service'] 	= $service;
		$data['images']		= $this->admin_model->get_service_images($service['id']);
		$data['active']		= 3;
		$this->load->view('page',$data);
	}
}
