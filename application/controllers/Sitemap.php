<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']		= 'Sitemap';
		$data['page']		= 'sitemap';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']		= $this->admin_model->get_metatags();
		$data['services']	= $this->admin_model->get_services();
		$data['galleries']	= $this->admin_model->get_galleries();
		$data['articles']	= $this->admin_model->get_articles();
		$data['active']		= 1;
		$this->load->view('page',$data);
	}
}
