<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	
	public function index()
	{
		$data['title']	= 'Testimonials';
		$data['page']	= 'testimonials';
		$data['settings']	= $this->admin_model->get_settings();
		$data['meta']	= $this->admin_model->get_metatags();
		$data['services']		= $this->admin_model->get_services();
		$data['testimonials']	= $this->admin_model->get_testimonials();
		$data['active']	= 5;
		$this->load->view('page',$data);
	}
}
