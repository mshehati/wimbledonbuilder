<?php
class Admin_model extends CI_Model {	
	
	public function login(){
		$usr	= $this->input->post('username');
		$pas	= $this->input->post('password');
		$user = $this->db->query("Select * from user where username ='$usr' and password ='$pas';")->result_array();
		if(count($user)>0){
			$this->session->set_userdata($user[0]);
			return true;
		}else{
			return false;
		}
	}

	public function change_password(){
		$old_pass	= $this->input->post('old_pass');
		$new_pass	= $this->input->post('new_pass');
		$user = $this->db->query("Select * from user where password ='$old_pass';")->result_array();
		if(count($user)>0){
			$this->db->where('id',$user[0]['id']);
			$this->db->update('user',array('password'=>$new_pass));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_slider_images(){
		return $this->db->query('Select * from slider_images order by priority')->result_array();
	}

	public function remove_image($id){
		$return = $this->db->get_where('slider_images',array('id'=>$id))->row_array();
		$this->db->query('delete from slider_images where id = ' . $id);
		unlink('uploads/slider/'.$return['filename']);
		return $return;
	}
	
	public function remove_service_image($id){
		$return = $this->db->get_where('service_images',array('id'=>$id))->row_array();
		$this->db->query('delete from service_images where id = ' . $id);
		unlink('uploads/services/'.$return['filename']);
		return $return;
	}
	
	public function remove_g_image($id){
		$return = $this->db->get_where('gallery_images',array('id'=>$id))->row_array();
		$this->db->query('delete from gallery_images where id = ' . $id);
		unlink('uploads/gallery/'.$return['filename']);
		return $return;
	}
	
	public function add_image($filename){
		$data = array(
			'filename'			=> $filename
		);
		$this->db->insert('slider_images',$data);
	}
	
	public function add_service_image($id,$filename){
		$data = array(
			'service_id'		=>$id,
			'filename'			=> $filename
		);
		$this->db->insert('service_images',$data);
	}
	
	public function get_services(){
		return $this->db->get("services")->result_array();
	}
	
	public function get_service($id){
		return $this->db->get_where("services",array('id'=>$id))->row_array();
	}
	
	public function get_service_t($title){
		$title_decoded = urldecode($title);
		return $this->db->get_where("services",array('hash_title'=>$title_decoded))->row_array();
	}
	
	public function add_service(){
		$data = array(
			'title'				=> $this->input->post('title'),
			'hash_title'		=> str_replace(' ', '_', $this->input->post('title')),
			'short_desc'		=> $this->input->post('short-desc'),	
			'full_desc'			=> $this->input->post('full-desc')
		);
		
		$this->db->insert('services',$data);		
	}
	
	public function add_gallery(){
		$type =  $this->input->post('type');
		$data = array(
			'title'				=> $this->input->post('title'),
			'hash_title'		=> str_replace(' ', '_', $this->input->post('title')),
			'type'				=> $type
		);
		
		$target_path = "uploads/gallery/"; 
   		if(isset($_FILES['image']['name'])){
   			$validextensions = array("jpeg", "jpg", "png");  
        	$ext = explode('.', basename($_FILES['image']['name'])); 
        	$file_extension = strtolower(end($ext)); 
        	$filename = md5(uniqid()) . "." . $ext[count($ext) - 1];
			if (($_FILES["image"]["size"] < 1000000) && in_array($file_extension, $validextensions)) {
            	if (move_uploaded_file($_FILES['image']['tmp_name'], $target_path.$filename)) {
            		$data['image'] = $filename;
            	} else {
                	$this->session->set_flashdata('error','Upload failed;');;
            	}
        	} else {
            	$this->session->set_flashdata('error','***Invalid file Size or Type***');
        	}
    	}
		if($type == 'video'){
			$data['video'] = $this->input->post('video_url');
		}
		$this->db->insert('galleries',$data);
		return $this->db->insert_id();
	}

	public function edit_gallery($id){
		$type =  $this->input->post('type');
		$data = array(
			'title'				=> $this->input->post('title'),
			'hash_title'		=> str_replace(' ', '_', $this->input->post('title')),
			'type'				=> $type
		);
		
		$target_path = "uploads/gallery/";
   		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!==''){
   			$validextensions = array("jpeg", "jpg", "png");  
        	$ext = explode('.', basename($_FILES['image']['name'])); 
        	$file_extension = strtolower(end($ext)); 
        	$filename = md5(uniqid()) . "." . $ext[count($ext) - 1];
			if (($_FILES["image"]["size"] < 1000000) && in_array($file_extension, $validextensions)) {
            	if (move_uploaded_file($_FILES['image']['tmp_name'], $target_path.$filename)) {
            		$data['image'] = $filename;
            	} else {
                	$this->session->set_flashdata('error','Upload failed;');;
            	}
        	} else {
            	$this->session->set_flashdata('error','***Invalid file Size or Type***');
        	}
    	}
		if($type == 'video'){
			$data['video'] = $this->input->post('video_url');
		}
		$this->db->where('id',$id);
		$this->db->update('galleries',$data);
	}
	
	public function edit_service($id){
		$data = array(
			'title'				=> $this->input->post('title'),
			'hash_title'		=> str_replace(' ', '_', $this->input->post('title')),
			'short_desc'		=> $this->input->post('short-desc'),	
			'full_desc'			=> $this->input->post('full-desc')
		);
		
		$this->db->where('id',$id);
		$this->db->update('services',$data);
	}
	
	public function remove_service($id){
		$service_imgs = $this->get_service_images($id);
		foreach($service_imgs as $img){
			unlink('uploads/services/'.$img['filename']);
		}
		$this->db->where('service_id',$id);
		$this->db->delete('service_images');
		
		$this->db->where('id',$id);
		$this->db->delete('services');
	}
	
	public function get_service_images($id){
		return $this->db->query('Select * from service_images where service_id = ' . $id . ' order by priority asc')->result_array();
	}
	
	public function get_testimonials(){
		return $this->db->query('Select * from testimonials order by priority asc')->result_array();
	}
	
	public function add_testimonial(){
		$data = array(
			'_date'		=> $this->input->post('date'),
			'location'	=> $this->input->post('location'),
			'people'	=> $this->input->post('people'),
			'content'	=> $this->input->post('content'),
			'hp'		=> $this->input->post('hp')=='on'?1:0
		);
		return $this->db->insert("testimonials", $data);
	}
	
	public function get_testimonial($id){
		
		return $this->db->get_where('testimonials',array('id'=>$id))->row_array();
	}
	
	public function edit_testimonial($id){
		$data = array(
			'_date'		=> $this->input->post('date'),
			'location'	=> $this->input->post('location'),
			'people'	=> $this->input->post('people'),
			'content'	=> $this->input->post('content'),
			'hp'		=> $this->input->post('hp')=='on'?1:0
		);
		$this->db->where('id',$id);
		$this->db->update('testimonials',$data);
	}
	
	public function remove_testimonial($id){
		$this->db->where('id',$id);	
		$this->db->delete('testimonials');
	}
	
	public function get_hp_testimonials(){
		return $this->db->query('Select * from testimonials where hp > 0 order by priority asc')->result_array();
	}
	
	public function get_galleries(){
		return $this->db->get('galleries')->result_array();
	}
	
	public function get_gallery($id){
		return $this->db->get_where('galleries',array('id'=>$id))->row_array();
	}
	
	public function get_gallery_h($h){
		return $this->db->get_where('galleries',array('hash_title'=>urldecode($h)))->row_array();
	}
	
	public function get_gallery_images($id){
		return $this->db->query("Select * from gallery_images where gallery_id=$id order by priority asc")->result_array();	
	}
	
	public function add_gallery_image($id,$filename){
		$data = array(
			'gallery_id'		=>$id,
			'filename'			=> $filename
		);
		$this->db->insert('gallery_images',$data);
	}
	
	public function remove_gallery($id){
		$service_imgs = $this->get_gallery_images($id);
		foreach($service_imgs as $img){
			unlink('uploads/gallery/'.$img['filename']);
		}
		$this->db->where('gallery_id',$id);
		$this->db->delete('gallery_images');
		
		$this->db->where('id',$id);
		$this->db->delete('galleries');
	}
	
	public function get_section($sec){
		return $this->db->get_where('sections',array('section'=>$sec))->row_array();
	}
	
	public function edit_section($sec){
		$data = array(
			'content'	=> $this->input->post('content')
		);
		$this->db->where('section',$sec);
		$this->db->update('sections',$data);
	}
	
	public function order_images(){
		foreach($_POST['id'] as $i =>$id){
			$data['priority'] = $_POST['priority'][$i] == '' ? 15 : $_POST['priority'][$i];
			
			$this->db->where('id',$id);
			$this->db->update('slider_images',$data);
			
		}
	}

	public function order_service_images($id){
		foreach($_POST['id'] as $i =>$id){
			$data['priority'] = $_POST['priority'][$i] == '' ? 15 : $_POST['priority'][$i];
			
			$this->db->where('id',$id);
			$this->db->update('service_images',$data);
			
		}
	}

	public function order_gallery_images($id){
		foreach($_POST['id'] as $i =>$id){
			$data['priority'] = $_POST['priority'][$i] == '' ? 15 : $_POST['priority'][$i];
			
			$this->db->where('id',$id);
			$this->db->update('gallery_images',$data);
			
		}
	}

	public function order_testimonials(){
		foreach($_POST['id'] as $i =>$id){
			$data['priority'] = $_POST['priority'][$i] == '' ? 10 : $_POST['priority'][$i];
			
			$this->db->where('id',$id);
			$this->db->update('testimonials',$data);
			
		}
	}

	public function get_metatags(){
		return $this->db->query("Select * from sections where section like 'meta_%' order by id")->result_array();
	}

	public function get_articles(){
		return $this->db->query('Select * from articles order by priority asc')->result_array();
	}

	public function add_article(){
		$data = array(
			'title'		=> $this->input->post('title'),
			'_date'		=> $this->input->post('date') === '' ? date('d M y') : $this->input->post('date'),
			'short'		=> $this->input->post('short'),
			'content'	=> $this->input->post('content'),
			'hp'		=> $this->input->post('hp')=='on'?1:0
		);
		return $this->db->insert("articles", $data);
	}

	public function edit_article($id){
		$data = array(
			'title'		=> $this->input->post('title'),
			'_date'		=> $this->input->post('date') === '' ? date('d M y') : $this->input->post('date'),
			'short'		=> $this->input->post('short'),
			'content'	=> $this->input->post('content'),
			'hp'		=> $this->input->post('hp')=='on'?1:0
		);
		$this->db->where('id',$id);
		$this->db->update('articles',$data);
	}

	public function get_article($id){
		return $this->db->get_where('articles',array('id'=>$id))->row_array();
	}

	public function remove_article($id){
		$this->db->where('id',$id);	
		$this->db->delete('articles');
	}

	public function order_articles(){
		foreach($_POST['id'] as $i =>$id){
			$data['priority'] = $_POST['priority'][$i] == '' ? 10 : $_POST['priority'][$i];
			
			$this->db->where('id',$id);
			$this->db->update('articles',$data);
			
		}
	}

	public function get_hp_articles(){
		return $this->db->query('Select * from articles where hp > 0 order by priority asc')->result_array();
	}

	//settings
	public function get_settings(){
		$settings 	= $this->db->get('settings')->result_array();
		$return		= array();

		foreach ($settings as $i => $s) {
			$return[$s['name']]	= array('val'=>$s['value'],'display'=>$s['display']); 
		}
		return $return;
	}

	public function get_settings_raw(){
		return $this->db->get('settings')->result_array();
	}

	public function update_setting($id){
		$data = array(
			'value'	=> $this->input->post('setting'),
			'display'		=> $this->input->post('display')=='on'?1:0
		);
		$this->db->where('id',$id);
		$this->db->update('settings',$data);
	}


	
	public function notify(){
		//TODO change email credentials
		include "PHPMailer/class.phpmailer.php";
		$mailer = new PHPMailer();
	    $mailer->IsSMTP();
		//$mailer->SMTPDebug  = 2;
	    $mailer->SMTPAuth = true;
	    //$mailer->SMTPSecure = "tls";
	    $mailer->Host = "mail.lcn.com";
	    $mailer->Port = 25;
	    $mailer->Username = "contact@wimbledonbuilder.co.uk";
	    $mailer->Password = "WB-contact1";
	    $mailer->SetFrom($this->input->post('email'), $this->input->post('name'));
	    $mailer->FromName = $this->input->post('name');
	    $mailer->AddAddress('wimbledonbuilder@gmail.com');
	    $mailer->Subject = 'Website Contact Form:' . $this->input->post('subject');
	    $msg = 'New message from the contact form.<br/>';
		$msg .= '<strong>Name: </strong>' .$this->input->post('name').'<br/>';
		$msg .= '<strong>Email: </strong>' .$this->input->post('email').'<br/>';
		$msg .= '<strong>Tel: </strong>' .$this->input->post('tel').'<br/>';
		$msg .= '<strong>Subject: </strong>' .$this->input->post('subject').'<br/>';
		$msg .= '<strong>Message: </strong>' .$this->input->post('msg').'<br/>'; 
	    
	    $mailer->Body = $msg;
	    $mailer->IsHTML (true);
	    if (!$mailer->Send()){
	    	return false;
		}
		return true;
	}
}