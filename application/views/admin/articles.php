<div>
	<a href="<?php echo site_url('admin/add_article');?>" class="btn btn-info pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
	<a href="<?php echo site_url('admin/order_articles');?>" class="btn btn-warning pull-right" style="margin-right:5px;"><span class="glyphicon glyphicon-sort-by-order"></span> Order</a>
	<h3>Articles</h3>
	<hr/>
	<?php if(count($articles)>0):?>
		<?php foreach($articles as $a):?>
			<div class="admin-testimonial row">
				<div class="col-xs-9">
					<h3><?php echo $a['title'];?></h3><?php if($a['hp']==1):?><span class="badge">Homepage</span><?php endif;?>
					<p><strong><i><?php echo $a['_date'];?></i></strong></p>
					<?php echo $a['short'];?>
				</div>
				<div class="col-xs-3">
					<a href="<?php echo site_url('admin/edit_article').'/'.$a['id'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
					<a href="<?php echo site_url('admin/remove_article').'/'.$a['id'];?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span> Delete</a>
				</div>
			</div>
			<hr />
		<?php endforeach; ?>
	<?php else: ?>
		<p>No articles available yet.</p>
	<?php endif; ?>
</div>