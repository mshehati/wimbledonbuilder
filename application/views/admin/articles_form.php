<div>
	<h3><?php echo $form;?> Article</h3>
	<hr/>
	<form action="<?php echo $action;?>" method="post">
  		<div class="form-group">
        <label for="exampleInputPassword1">Title</label>
        <input class="form-control" id="exampleInputPassword1" placeholder="Article Title" name="title" required="" value="<?php echo @$article['title'];?>"/>
      </div>
      <div class="form-group">
    		<label for="exampleInputEmail1">Date</label>
    		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Date" name="date" value="<?php echo @$article['_date'];?>">
  		</div>
      <div class="form-group">
        <label for="exampleInputEmail2">Short Description</label>
        <textarea name="short" class="form-control" rows="5"> <?php echo @$article['short'];?></textarea>
      </div>
  		<div class="form-group">
    		<label for="exampleInputFile">Full Article</label>
  			<div id="ng-app" ng-app="textAngularDemo" ng-controller="demoController" class="ng-scope">
				<div class="lighter">
					<div text-angular ng-model="htmlContent" name="content" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
				</div>
			</div>
			<script type="text/javascript">
				angular.module("textAngularDemo", ['textAngular']);
				function demoController($scope) {
					$scope.htmlContent = '<?php echo @$article["content"];?>';
				};
			</script>
		</div>
		<div class="checkbox">
    		<label>
      			<input type="checkbox" <?php if(!(@$article['hp']== 0)){echo 'checked';}?> name="hp"> Homepage
    		</label>
  		</div>
  		<button type="submit" class="btn btn-info"><?php echo $button;?></button>
	</form>
</div>