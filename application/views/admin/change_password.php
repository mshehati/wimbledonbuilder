<div>
	<h3>Change Password</h3>
	<hr/>
	<form class="form-changepass" action="<?php echo site_url('admin/change_password');?>" method="post">
       	<div class="form-group">	
       		<label for="old" >Old Password</label>
       		<input type="password" id="old" class="form-control" placeholder="Old Password" required="" autofocus="" name='old_pass'>
       	</div>
       	<div class="form-group">
       		<label for="newPass" >New Password</label>
       		<input type="password" id="newPass" class="form-control" placeholder="Password" required="" name="new_pass">
       	</div>
       	<div class="form-group">	
       		<label for="repeatPass" >Repeat Password</label>
       		<input type="password" id="repeatPass" class="form-control" placeholder="Password" required="" name="">
      	</div>	
     	<button class="btn btn-lg btn-primary btn-block" type="submit">Change Password</button>
   	</form>
	<script>
		$('.form-changepass').submit(function(){
			var pass 		= $('#newPass').val();
			var repeat_pass	= $('#repeatPass').val();

			if(pass !== repeat_pass){
				alert('Repaet password does not match. Please try again.');
				return false;
			}
		});
	</script>
</div>