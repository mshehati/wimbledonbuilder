<div>
	<a href="<?php echo site_url('admin/add_gallery');?>" class="btn btn-info pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
	<h3>Galleries</h3>
	<hr/>
	<?php $counter = 0; ?>
	<?php if(count($galleries)>0):?>
		<?php foreach($galleries as $g):?>
			<?php if($counter%3 == 0):?>
				<div class="row">
			<?php endif; ?>
			<div class="col-xs-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="gallery-cover">
							<img src="<?php echo base_url('uploads/gallery').'/'.$g['image'];?>"/>
							<hr/>
						</div>
						<h3 class="panel-title">
							<?php echo $g['title'];?>(<?php echo $g['type'];?>)
						</h3>
						
					</div>
					<div class="panel-footer text-center">
						<a href="<?php echo site_url('admin/edit_gallery').'/'.$g['id'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a> <a href="<?php echo site_url('admin/remove_gallery') . '/' . $g['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span> Delete</a><br/><br/>
						<?php if($g['type']=='image'):?>
							<a href="<?php echo site_url('admin/gallery_images').'/'.$g['id'];?>" class="btn btn-primary"><span class="glyphicon glyphicon-img"></span> Manage Gallery Images</a>
						<?php endif;?>
					</div>
				</div>
			</div>
			<?php if($counter%3 == 2 || $counter >= count($galleries)-1):?>
				</div>
			<?php endif; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<p>
			No galleries available yet.
		</p>
	<?php endif; ?>
</div>