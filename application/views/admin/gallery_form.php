<div>
	<h3><?php echo $form;?> Gallery</h3>
	<hr/>
	<form action="<?php echo $action;?>" method="post" enctype="multipart/form-data">
  		<div class="form-group">
    		<label for="exampleInputEmail1">Title</label>
    		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Gallery Title" name="title" required="" value="<?php echo @$gallery['title'];?>">
  		</div>
  		<div class="form-group">
    		<label for="exampleInputPassword1">Cover Image</label>
    		<input type="file"class="form-control" name="image"/><br/>
    		<?php if($form=='Edit'):?>
    			<strong>Current Image:</strong>
    			<img src="<?php echo base_url('uploads/gallery') .'/'.$gallery['image'];?>" style="width:200px;display:block;" />
    		<?php endif;?>
  		</div>
  		<div class="form-group">
    		<label for="exampleInputFile">Type:</label>
  			<div class="radio">
  				<label><input class="type" type="radio" name="type" id="optionsRadios1" value="image" <?php if(!isset($gallery)||@$gallery['type']=='image'){echo 'checked';}?>>Image Gallery</label>
			</div>
			<div class="radio">
  				<label><input class="type" type="radio" name="type" id="optionsRadios2" value="video" <?php if(@$gallery['type']==='video'){echo 'checked';}?>>Video Gallery</label>
			</div>
		</div>
		<div class="form-group" id="video-url" style="display:none" >
    		<label for="exampleInputPassword1">Video Url</label>
    		<input type="text" name="video_url" class="form-control" value='<?php echo @$gallery['video'];?>'/>
  		</div>
  		<button type="submit" class="btn btn-info"><?php echo $button;?></button>
	</form>
	<script>
		if($('.type:checked').val() == 'video'){
			$('#video-url').show();
		}
		$('.type').change(function(){
			console.log($('.type:checked').val());
			if($('.type:checked').val() == 'video'){
				$('#video-url').show();
			}else{
				$('#video-url').hide();
			}
		});
	</script>
</div>