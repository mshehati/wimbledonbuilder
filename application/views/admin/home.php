<div>
	<h3>Home</h3>
	<hr/>
	<div id="content" class="text-edit">
		<?php echo $about['content'];?>
		<br/>
		<a href="#" class="switch btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
	</div>
	<div id="about-form" style='display: none;' class="text-edit">
		<form action="" method="post">
  			<div class="form-group">
    			<div id="ng-app" ng-app="textAngularDemo" ng-controller="demoController" class="ng-scope">
					<div class="lighter">
						<div text-angular ng-model="htmlContent" name="content" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
					</div>
				</div>
				<script type="text/javascript">
					angular.module("textAngularDemo", ['textAngular']);
					function demoController($scope) {
						$scope.htmlContent = '<?php echo $about['content'];?>';
					};
				</script>
			</div>
  			<button type="submit" class="btn btn-info">Save Changes</button>
  			<a href="#" class="switch btn btn-warning"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</a>
		</form>
	</div>
	<script>
		$('.switch').click(function(){
			$('.text-edit').toggle();
			return false;
		});
	</script>
</div>