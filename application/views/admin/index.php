<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- title & seo -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?></title>
<meta name="robots" content="index, follow">

<!-- style and scripts -->
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/bootstrap-theme.css">
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/style.css">	
<script src="<?php echo base_url('style/js');?>/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url('style/js');?>/bootstrap.js"></script>
<script src="<?php echo base_url('style');?>/js/form.js"></script>

<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/textAngular/1.2.0/textAngular-sanitize.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/textAngular/1.2.0/textAngular.min.js" type="text/javascript"></script>
</head>
<body role="document">
	<div class="container text-center">
		<h1>Adminstration Panel</h1>
		<hr/>
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
	      		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	      		<?php echo $this->session->flashdata('success');?>
	    	</div>
		<?php elseif($this->session->flashdata('error')):?>
			<div class="alert alert-danger alert-dismissible fade in" role="alert">
	      		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	      		<?php echo $this->session->flashdata('error');?>
	    	</div>
		<?php endif;?>
	</div>
	<?php if($logged):?>
	<div class="container" role="main">
		<div class='row'>
			<div class="col-xs-3">
				<div class="panel panel-default">
					<div class="panel-heading text-center">
						<h3 class="panel-title">Menu</h3>
					</div>
					<div class="panel-body">
						<ul class="nav nav-pills nav-stacked">
							<li role="presentation" <?php if($active == 1){echo 'class="active"';}?>><a href="<?php echo site_url('admin/slider_images');?>">Homepage slider images</a></li>
							<li role="presentation" <?php if($active == 2){echo 'class="active"';}?>><a href="<?php echo site_url('admin/home');?>">Home</a></li>
							<li role="presentation" <?php if($active == 3){echo 'class="active"';}?>><a href="<?php echo site_url('admin/about');?>">About Us</a></li>
  							<li role="presentation" <?php if($active == 4){echo 'class="active"';}?>><a href="<?php echo site_url('admin/services');?>">Services</a></li>
  							<li role="presentation" <?php if($active == 5){echo 'class="active"';}?>><a href="<?php echo site_url('admin/testimonials');?>">Testimonials</a></li>
  							<li role="presentation" <?php if($active == 6){echo 'class="active"';}?>><a href="<?php echo site_url('admin/galleries');?>">Galleries</a></li>
  							<li role="presentation" <?php if($active == 7){echo 'class="active"';}?>><a href="<?php echo site_url('admin/meta');?>">Meta Tag</a></li>
  							<li role="presentation" <?php if($active == 8){echo 'class="active"';}?>><a href="<?php echo site_url('admin/articles');?>">Articles</a></li>
  							<li role="presentation" <?php if($active == 9){echo 'class="active"';}?>><a href="<?php echo site_url('admin/settings');?>">Site settings</a></li>
						</ul>
					</div>
					<div class="panel-footer text-center">
						<a href="<?php echo site_url('admin/logout');?>" class="btn btn-danger"><span class="glyphicon glyphicon-off"></span> Sign Out</a>
						<hr />
						<a href="<?php echo site_url('admin/change_password');?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span> Change Password</a>
						<hr/>
						&copy; Wimbledon Builder.
					</div>
				</div>
			</div>
			<div class="col-xs-9">
				<?php include $page.'.php';?>		
			</div>
		</div>
	</div>
	<?php else:?>
	<div class="container">
		<form class="form-signin" action="<?php echo site_url('admin/login');?>" method="post">
       		<h2 class="form-signin-heading">Please sign in</h2>
       		<label for="inputEmail" class="sr-only">Email address</label>
       		<input type="text" id="inputEmail" class="form-control" placeholder="Username" required="" autofocus="" name='username'>
       		<label for="inputPassword" class="sr-only">Password</label>
       		<input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">
      		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
   		</form>
    </div>
	<?php endif;?>
</body>
</html>