<div>
	<h3>Meta Tags</h3>
	<hr/>
	<div id="content" class="text-edit-title">
		<h4>Meta Title</h4>
		<div class="row">
			<div class="col-sm-10">
				<?php echo $meta[0]['content'];?>
			</div>
			<div class="col-sm-2">
				<a href="#" class="switch-title btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
			</div>
		</div>	
	</div>
	<div id="meta-title-form" style='display: none;' class="text-edit-title">
		<form action="<?php echo site_url('admin/meta/title');?>" method="post">
  			<div class="row">
  				<div class="col-sm-10">
	  				<div class="form-group">
    					<textarea name="content" class="form-control"><?php echo $meta[0]['content'];?></textarea>
  					</div>
				</div>
				<div class="col-sm-2 text-center">
  					<button type="submit" class="btn btn-info" style="margin-bottom:5px;">Save Changes</button><br/>
  					<a href="#" class="switch-title btn btn-warning"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</a>
  				</div>
  			</div>
		</form>
	</div>

	<hr/>
	<div id="content" class="text-edit-desc">
		<h4>Meta Description</h4>
		<div class="row">
			<div class="col-sm-10">
				<?php echo $meta[1]['content'];?>
			</div>
			<div class="col-sm-2">
				<a href="#" class="switch-desc btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
			</div>
		</div>	
	</div>
	<div id="meta-title-form" style='display: none;' class="text-edit-desc">
		<form action="<?php echo site_url('admin/meta/description');?>" method="post">
  			<div class="row">
  				<div class="col-sm-10">
	  				<div class="form-group">
    					<textarea name="content" class="form-control"><?php echo $meta[1]['content'];?></textarea>
  					</div>
				</div>
				<div class="col-sm-2 text-center">
  					<button type="submit" class="btn btn-info" style="margin-bottom:5px;">Save Changes</button><br/>
  					<a href="#" class="switch-desc btn btn-warning"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</a>
  				</div>
  			</div>
		</form>
	</div>

	<hr/>
	<div id="content" class="text-edit-keywords">
		<h4>Meta Title</h4>
		<div class="row">
			<div class="col-sm-10">
				<?php echo $meta[2]['content'];?>
			</div>
			<div class="col-sm-2">
				<a href="#" class="switch-keywords btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
			</div>
		</div>	
	</div>
	<div id="meta-title-form" style='display: none;' class="text-edit-keywords">
		<form action="<?php echo site_url('admin/meta/keywords');?>" method="post">
  			<div class="row">
  				<div class="col-sm-10">
	  				<div class="form-group">
    					<textarea name="content" class="form-control"><?php echo $meta[2]['content'];?></textarea>
  					</div>
				</div>
				<div class="col-sm-2 text-center">
  					<button type="submit" class="btn btn-info" style="margin-bottom:5px;">Save Changes</button><br/>
  					<a href="#" class="switch-keywords btn btn-warning"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</a>
  				</div>
  			</div>
		</form>
	</div>

	<script>
		$('.switch-title').click(function(){
			$('.text-edit-title').toggle();
			return false;
		});
		$('.switch-desc').click(function(){
			$('.text-edit-desc').toggle();
			return false;
		});
		$('.switch-keywords').click(function(){
			$('.text-edit-keywords').toggle();
			return false;
		});
	</script>
</div>