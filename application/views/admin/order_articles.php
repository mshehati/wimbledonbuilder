<div>
	<h3>Order Articls</h3>
	<hr/>
	<?php if(count($articles)>0):?>
		<form action="<?php echo site_url('admin/order_articles');?>" method="post">
			<?php foreach($articles as $a):?>
				<div class="admin-testimonial row">
					<div class="col-xs-9">
						<h3><?php echo $a['title'];?></h3><?php if($a['hp']==1):?><span class="badge">Homepage</span><?php endif;?>
						<p><strong><i><?php echo $a['_date'];?></i></strong></p>
						<?php echo $a['short'];?>
					</div>
					<div class="col-xs-3">
						<strong>Priority:</strong><br/>
						<input type="hidden" name="id[]" value="<?php echo $a['id'];?>" />
						<input class="form-control" type="number" value="<?php echo $a['priority'];?>" name="priority[]" placeholder="Priority" min="0" max="100"  />
					</div>
				</div>
				<hr />
			<?php endforeach; ?>
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</form>
	<?php else: ?>
		<p>
			No images uploaded. 
		</p>
	<?php endif; ?>
</div>