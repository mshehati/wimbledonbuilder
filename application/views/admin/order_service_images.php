<div> 
	<h3><?php echo $service['title'];?> | Order Images</h3>
	<hr/>
	<?php if(count($images)>0):?>
		<form action="<?php echo site_url('admin/order_service_images').'/'.$id;?>" method="post">
			<?php foreach($images as $image):?>
				<div class="row">
					<div class="col-xs-9">
						<img src="<?php echo base_url('uploads/services') . '/' . $image['filename']; ?>" height="100px" />
					</div>
					<div class="col-xs-3">
						<strong>Priority:</strong><br/>
						<input type="hidden" name="id[]" value="<?php echo $image['id'];?>" />
						<input class="form-control" type="number" value="<?php echo $image['priority'];?>" name="priority[]" placeholder="Priority" min="0" max="100"  />
					</div>
				</div>
				<hr/>
			<?php endforeach; ?>
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</form>
	<?php else: ?>
		<p>
			No images uploaded. 
		</p>
	<?php endif; ?>
</div>