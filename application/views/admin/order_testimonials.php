<div>
	<h3>Order Images</h3>
	<hr/>
	<?php if(count($testimonials)>0):?>
		<form action="<?php echo site_url('admin/order_testimonials');?>" method="post">
			<?php foreach($testimonials as $t):?>
				<div class="admin-testimonial row">
					<div class="col-xs-9">
						<h3><?php echo $t['people'];?></h3><?php if($t['hp']==1):?><span class="badge">Homepage</span><?php endif;?>
						<p><strong><i><?php echo $t['_date'];?></i></strong> - <u><?php echo $t['location'];?></u></p>
						<?php echo $t['content'];?>
					</div>
					<div class="col-xs-3">
						<strong>Priority:</strong><br/>
						<input type="hidden" name="id[]" value="<?php echo $t['id'];?>" />
						<input class="form-control" type="number" value="<?php echo $t['priority'];?>" name="priority[]" placeholder="Priority" min="0" max="100"  />
					</div>
				</div>
				<hr />
			<?php endforeach; ?>
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</form>
	<?php else: ?>
		<p>
			No images uploaded. 
		</p>
	<?php endif; ?>
</div>