<div>
	<a href="<?php echo site_url('admin/order_service_images').'/'.$service['id'];?>" class="btn btn-warning pull-right"><span class="glyphicon glyphicon-sort-by-order"></span> Order Images</a>
	<h3><?php echo $service['title'];?> | Images</h3>
	<hr/>
	<?php $counter = 0; ?>
	<?php if(count($images)>0):?>
		<?php foreach($images as $image):?>
			<?php if($counter%4 == 0):?>
				<div class="row">
			<?php endif; ?>
			<div class="col-xs-3">
				<img src="<?php echo base_url('uploads/services') . '/' . $image['filename']; ?>" width="100%" />
				<hr/>
				<a href="<?php echo site_url('admin/remove_service_image') . '/' . $image['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span> Delete</a>
			</div>
			<?php if($counter%4 == 3 || $counter >= count($images)-1):?>
				</div>
				<hr />
			<?php endif; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<p>
			No images uploaded. Use the form below to upload images for the homepage slider.
		</p>
	<?php endif; ?>
	<hr />
</div>
<div class="row">
	<div class="well">
		<div id="formdiv">
			<h3>Uplaod Images</h3>
			<form enctype="multipart/form-data" action="" method="post">
				Only JPEG,PNG,JPG Type Image Uploaded. Image Size Should Be Less Than 1MB.</br>
				
				<hr/>
				<div id="filediv">
					<input name="file[]" type="file" id="file" class="btn btn-default" />
				</div>
				<br/>
				<input type="button" id="add_more" class="upload btn btn-success" value="Add More Files"/>
				<input type="submit" value="Upload File" name="submit" id="upload" class="upload btn btn-primary"/>
			</form>
			<br/>
			<br/>
		</div>
	</div>
</div>