<div>
	<a href="<?php echo site_url('admin/add_service');?>" class="btn btn-info pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
	<h3>Services</h3>
	<hr/>
	<?php $counter = 0; ?>
	<?php if(count($services)>0):?>
		<?php foreach($services as $service):?>
			<?php if($counter%3 == 0):?>
				<div class="row">
			<?php endif; ?>
			<div class="col-xs-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<h3 class="panel-title">
							<?php echo $service['title'];?>
						</h3>
					</div>
					<div class="panel-footer text-center">
						<a href="<?php echo site_url('admin/edit_service').'/'.$service['id'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a> <a href="<?php echo site_url('admin/remove_service') . '/' . $service['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span> Delete</a><br/><br/>
						<a href="<?php echo site_url('admin/service_images').'/'.$service['id'];?>" class="btn btn-primary"><span class="glyphicon glyphicon-img"></span> Manage Service Images</a>
					</div>
				</div>
			</div>
			<?php if($counter%3 == 2 || $counter >= count($services)-1):?>
				</div>
			<?php endif; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<p>
			No services available yet.
		</p>
	<?php endif; ?>
</div>