<div>
	<h3><?php echo $form;?> Services</h3>
	<hr/>
	<form action="<?php echo $action;?>" method="post">
  		<div class="form-group">
    		<label for="exampleInputEmail1">Title</label>
    		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Service Title" name="title" required="" value="<?php echo @$service['title'];?>">
  		</div>
  		<div class="form-group">
    		<label for="exampleInputPassword1">Short Description</label>
    		<textarea class="form-control" id="exampleInputPassword1" placeholder="Short description" maxlength="250" name="short-desc" rows="3"><?php echo @$service['short_desc'];?></textarea>
  		</div>
  		<div class="form-group">
    		<label for="exampleInputFile">Full Description</label>
  			<div id="ng-app" ng-app="textAngularDemo" ng-controller="demoController" class="ng-scope">
				<div class="lighter">
					<div text-angular ng-model="htmlContent" name="full-desc" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
				</div>
			</div>
			<script type="text/javascript">
				angular.module("textAngularDemo", ['textAngular']);
				function demoController($scope) {
					$scope.htmlContent = "<?php echo @$service['full_desc'];?>";
				};
			</script>
		</div>
  		<button type="submit" class="btn btn-info"><?php echo $button;?></button>
	</form>
</div>