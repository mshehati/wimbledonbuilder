<div>
	<h3>Settings</h3>
	<hr/>
	<?php foreach($settings as $s):?>
		<form action="<?php echo site_url('admin/update_setting') . '/' . $s['id'];?>" method="post">
			<div class="row">
				<div class="col-sm-2"><label><?php echo $s['name'];?></label></div>
				<div class="col-sm-6"><textarea class="form-control" name="setting"><?php echo $s['value'];?></textarea></div>
				<div class="col-sm-2">
	  				<div class="checkbox">
	    				<label>
	      					<input type="checkbox" <?php if(!($s['display']== 0)){echo 'checked';}?> name="display"> Display
	    				</label>
	  				</div>
	  			</div>
	  			<div class="col-sm-2">
	  				<button type="submit" class="btn btn-success">Update</button>
	  			</div>
	  		</div>
		</form>
		<hr/>
	<?php endforeach; ?>
</div>