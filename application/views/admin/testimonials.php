<div>
	<a href="<?php echo site_url('admin/add_testimonial');?>" class="btn btn-info pull-right"><span class="glyphicon glyphicon-plus"></span> Add</a>
	<a href="<?php echo site_url('admin/order_testimonials');?>" class="btn btn-warning pull-right" style="margin-right:5px;"><span class="glyphicon glyphicon-sort-by-order"></span> Order</a>
	<h3>Testimonials</h3>
	<hr/>
	<?php if(count($testimonials)>0):?>
		<?php foreach($testimonials as $t):?>
			<div class="admin-testimonial row">
				<div class="col-xs-9">
					<h3><?php echo $t['people'];?></h3><?php if($t['hp']==1):?><span class="badge">Homepage</span><?php endif;?>
					<p><strong><i><?php echo $t['_date'];?></i></strong> - <u><?php echo $t['location'];?></u></p>
					<?php echo $t['content'];?>
				</div>
				<div class="col-xs-3">
					<a href="<?php echo site_url('admin/edit_testimonial').'/'.$t['id'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
					<a href="<?php echo site_url('admin/remove_testimonial').'/'.$t['id'];?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><span class="glyphicon glyphicon-trash"></span> Delete</a>
				</div>
			</div>
			<hr />
		<?php endforeach; ?>
	<?php else: ?>
		<p>No testimonials available yet.</p>
	<?php endif; ?>
</div>