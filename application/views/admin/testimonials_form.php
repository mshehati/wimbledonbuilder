<div>
	<h3><?php echo $form;?> Testimonial</h3>
	<hr/>
	<form action="<?php echo $action;?>" method="post">
  		<div class="form-group">
    		<label for="exampleInputEmail1">Date</label>
    		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Date" name="date" value="<?php echo @$testimonial['_date'];?>">
  		</div>
  		<div class="form-group">
    		<label for="exampleInputEmail2">Location</label>
    		<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Location" name="location" value="<?php echo @$testimonial['location'];?>">
  		</div>
  		<div class="form-group">
    		<label for="exampleInputPassword1">People</label>
    		<input class="form-control" id="exampleInputPassword1" placeholder="People" name="people" required="" value="<?php echo @$testimonial['people'];?>"/>
  		</div>
  		<div class="form-group">
    		<label for="exampleInputFile">Content</label>
  			<div id="ng-app" ng-app="textAngularDemo" ng-controller="demoController" class="ng-scope">
				<div class="lighter">
					<div text-angular ng-model="htmlContent" name="content" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
				</div>
			</div>
			<script type="text/javascript">
				angular.module("textAngularDemo", ['textAngular']);
				function demoController($scope) {
					$scope.htmlContent = "<?php echo @$testimonial['content'];?>";
				};
			</script>
		</div>
		<div class="checkbox">
    		<label>
      			<input type="checkbox" <?php if(!(@$testimonial['hp']== 0)){echo 'checked';}?> name="hp"> Homepage
    		</label>
  		</div>
  		<button type="submit" class="btn btn-info"><?php echo $button;?></button>
	</form>
</div>