	<nav class="navbar navbar-inverse navbar-custom">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p class="navbar-brand visible-sm visible-xs"><a href="mailto:<?php echo $settings['email']['val'];?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo $settings['email']['val'];?></a><br/>
				<a href="tel:<?php echo $settings['tel']['val'];?>"><span class="glyphicon glyphicon-phone-alt"></span> <?php echo $settings['tel']['val'];?></a> | <a href="tel:<?php echo $settings['mobile']['val'];?>"><span class="glyphicon glyphicon-phone"></span> <?php echo $settings['mobile']['val'];?></a></p>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li <?php if($active === 1){echo 'class="active"';}?>><a href="<?php echo site_url();?>">Home</a></li>
					<li <?php if($active === 2){echo 'class="active"';}?>><a href="<?php echo site_url('about');?>">About Us</a></li>
					<li class="dropdown <?php if($active === 3){echo "active";}?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
						 <ul class="dropdown-menu custom-style">
						 	<li><a href="<?php echo site_url('services');?>">All Services</a></li>
							<?php foreach($services as $m_s):?>
								<li><a href="<?php echo site_url('services').'/s/'.urlencode($m_s['hash_title']);?> "><?php echo $m_s['title'];?></a></li>
							<?php endforeach;?>
						 </ul>

					</li>
              		<li <?php if($active === 4){echo 'class="active"';}?>><a href="<?php echo site_url('gallery');?>">Gallery</a></li>
              		<li <?php if($active === 5){echo 'class="active"';}?>><a href="<?php echo site_url('testimonials');?>">Testimonials</a></li>
              		<li <?php if($active === 6){echo 'class="active"';}?>><a href="<?php echo site_url('contact');?>">Contact Us</a></li>
            	</ul>
            	<ul class="nav navbar-nav navbar-right hidden-sm hidden-xs contact">
            		<li><a href="mailto:<?php echo $settings['email']['val'];?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo $settings['email']['val'];?></a></li>
            		<li><a href="tel:<?php echo $settings['tel']['val'];?>"><span class="glyphicon glyphicon-phone-alt"></span> <?php echo $settings['tel']['val'];?></a></li>
            		<li><a href="tel:<?php echo $settings['mobile']['val'];?>"><span class="glyphicon glyphicon-phone"></span> <?php echo $settings['mobile']['val'];?></a></li>
            	</ul>
            </div><!--/.nav-collapse -->
		</div>
	</nav>