<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- title & seo -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title .' | ' . $settings['site_title']['val'];?> </title>
<meta name="title" content="<?php echo $meta[0]['content'];?>">
<meta name="description" content="<?php echo $meta[1]['content'];?>">
<meta name="robots" content="index, follow">
<meta name="keywords" content="<?php echo $meta[2]['content'];?>">

<!-- style and scripts -->
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/bootstrap-theme.css">
<link rel="stylesheet" href="<?php echo base_url('style/css');?>/style.css">	
<script src="<?php echo base_url('style/js');?>/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url('style/js');?>/bootstrap.js"></script>

</head>
<body role="document">
	<header class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 logo">
					<h1 class="page-title"><a href="<?php echo site_url();?>"><?php echo $settings['site_title']['val'];?></a></h1>
					<h2 style="color: transparent; float:left; font-size:1px; margin:0; padding:0;"><?php echo $settings['seo_text']['val'];?></h2>
				</div>
			</div>
		</div>
	</header>
<?php include 'inc/menu.php';?>
<?php include 'pages/'.$page.'.php';?>
	<footer>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 hidden-xs hidden-sm">
    				<h3>About Us</h3>
    				<?php echo nl2br($settings['about_us']['val']);?>
    			</div>
    			<div class="col-md-3 col-sm-6">
    				<h3>Services</h3>
    				<ul>
    					<?php foreach($services as $s):?>
    						<li><a href="<?php echo site_url('services').'/s/'.urlencode($s['hash_title']);?>"><?php echo $s['title'];?></a></li>
    					<?php endforeach;?>
    				</ul>
    			</div>
    			<div class="col-md-3 col-sm-6">
    				<h3>Contact</h3>
                    <?php if($settings['address']['display'] == '1'):?>
                        <p><span class="glyphicon glyphicon-map-marker"></span> <?php echo $settings['address']['val'];?></p>
    				<?php endif;?>
                    <a href="mailto:<?php echo $settings['email']['val'];?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo $settings['email']['val'];?></a><br/>
					<a href="tel:<?php echo $settings['tel']['val'];?>"><span class="glyphicon glyphicon-phone-alt"></span> <?php echo $settings['tel']['val'];?></a><br/>
					<a href="tel:<?php echo $settings['mobile']['val'];?>"><span class="glyphicon glyphicon-phone"></span> <?php echo $settings['mobile']['val'];?></a>
    			</div>
    		</div>
    		<div class="row">
    			<hr />
	    		<div class="copyright">
	    			&copy; Wimbledon Builders <?php echo date('Y');?>. All rights reserved. | <a href="<?php echo site_url('sitemap');?>">Sitemap</a>
	    		</div>
	    	</div>
    	</div>
    </footer>
    <script>
    	$(document).on('contextmenu','img',function(){return false;});
    </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35217218-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>