
	<div class="container wrapper" role="main">
		<div class='row'>
			<div class='col-md-6 col-md-push-6'>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php foreach ($slider_images as $key => $img):?>
		          			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $key;?>" class="<?php if($key<1){echo'active';}?>"></li>
		          		<?php endforeach;?>
		        	</ol>
		        	<div class="carousel-inner" role="listbox">
		        		<?php foreach ($slider_images as $key => $img):?>
		        			<div class="item <?php if($key<1){echo'active';}?>">
	      						<img src="<?php echo base_url('uploads/slider').'/'.$img['filename'];?>" alt="...">
	      						<div class="carousel-caption">
	        						...
	      						</div>
	    					</div>
		          		<?php endforeach;?>
  					</div>
		    	    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		        	  	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		          		<span class="sr-only">Previous</span>
			        </a>
		    	    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		        	  	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		          		<span class="sr-only">Next</span>
			        </a>
				</div>
			</div>
			<div class="col-md-6 col-md-pull-6">
				<?php echo $about['content'];?>
			</div>
		</div>
		<div class="page-header">
			<h2>Testimonials</h2>
		</div>		
    	<div class="testimonials">
    		<?php foreach ($testimonials as $i => $t):?>
    			<div class="testimonial">
    				<span class="testimonial-head"><?php echo $t['people'];?></span>
    				<span class='testimonial-date'><?php echo $t['_date'];?> <?php echo $t['location']!== '' ? '| '.$t['location']: ''; ?></span> 
					<div class="testimonial-content"><?php echo $t['content'];?></div>
    			</div>
    		<?php endforeach;?>
    	</div>
    </div> <!-- /container -->