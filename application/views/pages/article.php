<div class="container wrapper" role="main">
	<div class='row'>
		<div class="col-md-12">
			<h2>
				<?php echo $article['title'];?>
			</h2>
			<hr/>
			<?php echo $article['content'];?>
		</div>
	</div>
	<hr/>
</div> <!-- /container -->