<div class="container wrapper" role="main">
	<div>
		<h2>Contact Us</h2>
	</div>
	<hr />
	<div class='row'>
		<div class='col-sm-6 contact-info'>
			<p><i>Please use the below provided contact information or the contact form on the right side if you have any questions or requests, concerning our services.</i></p>
			<br/>
			<?php if($settings['address']['display']==1):?>
				<p class="lead">
					<span class="glyphicon glyphicon-map-marker"></span> <?php echo $settings['address']['val'];?>
				</p>
			<?php endif;?>
			<p class="lead">
				<a hef="mailto:<?php echo $settings['email']['val'];?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo $settings['email']['val'];?></a> 
			</p>
			<p class="lead">
				<a href="tel:<?php echo $settings['tel']['val'];?>"><span class="glyphicon glyphicon-phone"></span> <?php echo $settings['tel']['val'];?></a>
			</p>
			<p class="lead">
				<a href="tel:<?php echo $settings['mobile']['val'];?>"><span class="glyphicon glyphicon-phone-alt"></span> <?php echo $settings['mobile']['val'];?></a>
			</p>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-custom">
				<div class="panel-heading">
					<h3 class="panel-title">Contact Form</h3>
				</div>
				<div class="panel-body">
					<?php if($this->session->flashdata('contact')):?>
						<div class="alert alert-success alert-dismissible" role="alert">
  							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  							<strong>Thank you for contacting us!</strong> We will get back at you as soon as possible.
						</div>
					<?php elseif($this->session->flashdata('contact-error')):?>
						<div class="alert alert-danger alert-dismissible" role="alert">
  							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  							<strong>Something went wrong!</strong> We will try to fix it as soon as possible. Please try again later or use the contact information provided to contact us.<br/>
  							Thank You!
						</div>
					<?php endif;?>
					<form class="form-horizontal" method="post" action="<?php echo site_url('contact/send');?>">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Full Name</label>
							<div class="col-sm-9">
		      					<input type="text" class="form-control" id="inputEmail3" placeholder="Full name" name="name" required="">
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<label for="inputPassword3" class="col-sm-3 control-label">Email</label>
		    				<div class="col-sm-9">
		      					<input type="email" class="form-control" id="inputPassword3" placeholder="Email" name="email" required="">
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<label for="inputPassword3" class="col-sm-3 control-label">Phone number</label>
		    				<div class="col-sm-9">
		      					<input type="tel" class="form-control" id="inputPassword3" placeholder="Phone number" name="tel">
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<label for="inputPassword3" class="col-sm-3 control-label">Subject</label>
		    				<div class="col-sm-9">
		      					<input type="text" class="form-control" id="inputPassword3" placeholder="Subject" name="subject" required="">
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<label for="inputPassword3" class="col-sm-3 control-label">Message</label>
		    				<div class="col-sm-9">
		      					<textarea class="form-control" rows="6" placeholder="Message" name="msg"></textarea>
		    				</div>
		  				</div>
		  				<div class="form-group">
		    				<div class="col-sm-offset-3 col-sm-9">
		      					<button type="submit" class="btn btn-default btn-custom">Send</button>
		    				</div>
		  				</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
	<?php if($settings['map']['display'] == 1): ?>
		<section>
			<div class="page-header">
				<h2>Our Location</h2>
			</div>
			<div class="map">
				<?php echo $settings['map']['val'];?>
			</div>
		</section>
	<?php endif;?>
	<hr />
</div> <!-- /container -->