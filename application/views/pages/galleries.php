<div class="container wrapper" role="main">
	<section class="service-listing">
		<h2>Galleries</h2>
		<hr/>
		<div class="row">
			<?php foreach($galleries as $g):?>
				<div class="col-md-3 col-sm-4 col-xs-6">
	          		<div class="panel panel-default panel-custom">
	            		<div class="panel-heading">
	              			<h2 class="panel-title"><?php echo $g['title'];?></h2>
	            		</div>
	            		<div class="panel-body hidden-xs">
	            			<div class="gallery-cover-img">
	            				<img src="<?php echo base_url('uploads/gallery') .'/'. $g['image'];?>"/>
	            			</div>
	            		</div>
	            		<div class="panel-footer">
	            			<a href="<?php echo site_url('gallery').'/view/'.urlencode($g['hash_title']);?> " class="btn btn-default btn-custom">View Gallery</a>
	            		</div>
	          		</div>
	      		</div>
			<?php endforeach;?>
      	</div>
	</section>
	<hr />
</div> <!-- /container -->