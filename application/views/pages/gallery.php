<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
			<div class="rg-image"></div>
			<div class="rg-loading"></div>
			<div class="rg-caption-wrapper">
				<div class="rg-caption" style="display:none;">
					<p></p>
				</div>
			</div>
	</div>
</script>
<div class="container wrapper single-gallery" role="main">
	<h2><?php echo $gallery['title'];?></h2>
	<hr />
	<?php if($gallery['type']=='image'):?>
	<div id="rg-gallery" class="rg-gallery">
   		<div class="rg-thumbs">
    		<div class="es-carousel-wrapper">
    			<div class="es-nav">
       				<span class="es-nav-prev">Previous</span>
       				<span class="es-nav-next">Next</span>
    			</div>
    			<div class="es-carousel">
       				<ul>
       					<?php foreach($images as $image):?>
                   			<li>
                   				<a href="#">
                   					<img src="<?php echo base_url('uploads') .'/gallery/' . $image['filename'];?>" data-large="<?php echo base_url('uploads/gallery') .'/' . $image['filename'];?>" alt="" data-description="<?php echo $image['description'];?>" />
                   				</a>
                           	</li>
						<?php endforeach; ?>
               		</ul>
           		</div>
       		</div>
   		</div>
   	</div><!-- rg-gallery -->
   	<?php else:?>
   		<?php echo $gallery['video'];?>
   	<?php endif;?>
   	<hr />
</div><!-- /container -->
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.tmpl.min.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.easing.1.3.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.elastislide.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/gallery.js';?>"></script>