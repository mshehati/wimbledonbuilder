
	<div class="container wrapper" role="main">
		<div class='row'>
			<div class='col-md-6 col-md-push-6'>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php foreach ($slider_images as $key => $img):?>
		          			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $key;?>" class="<?php if($key<1){echo'active';}?>"></li>
		          		<?php endforeach;?>
		        	</ol>
		        	<div class="carousel-inner" role="listbox">
		        		<?php foreach ($slider_images as $key => $img):?>
		        			<div class="item <?php if($key<1){echo'active';}?>">
	      						<img src="<?php echo base_url('uploads/slider').'/'.$img['filename'];?>" alt="...">
	      						<div class="carousel-caption">
	        						...
	      						</div>
	    					</div>
		          		<?php endforeach;?>
  					</div>
		    	    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		        	  	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		          		<span class="sr-only">Previous</span>
			        </a>
		    	    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		        	  	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		          		<span class="sr-only">Next</span>
			        </a>
				</div>
			</div>
			<div class="col-md-6 col-md-pull-6">
				<?php echo $home['content'];?>
			</div>
		</div>
		<section class="service-listing">
			<div class="page-header">
				<h2>Services</h2>
			</div>
			<div class="row">
				<?php foreach($services as $i=> $service):?>
					<div class="col-md-4 col-sm-6">
		          		<div class="panel panel-default panel-custom">
		            		<div class="panel-heading">
		              			<h2 class="panel-title"><?php echo $service['title'];?></h2>
		            		</div>
		            		<div class="panel-body">
		              			<?php echo $service['short_desc'];?>
		            		</div>
		            		<div class="panel-footer">
		            			<a href="<?php echo site_url('services').'/s/'.urlencode($service['hash_title']);?> " class="btn btn-default btn-custom">Find out more</a>
		            		</div>
		          		</div>
		      		</div>
				<?php endforeach;?>
	      	</div>
		</section>
		<div class="page-header">
			<h2>News</h2>
		</div>		
    	<div class="testimonials">
    		<?php foreach ($articles as $i => $a):?>
    			<div class="testimonial">
    				<span class="testimonial-head"><a href="<?php echo site_url('articles') . '/a/' . $a['id'];?>"><?php echo $a['title'];?></a></span>
    				<span class='testimonial-date'><?php echo $a['_date'];?></span> 
					<div class="testimonial-content article">
						<?php echo $a['short'];?>
						<div class="text-right">
							<a href="<?php echo site_url('articles') . '/a/' . $a['id'];?>" class="btn btn-default btn-custom">Read more...</a>
						</div>
					</div>
    			</div>
    		<?php endforeach;?>
    	</div>
    </div> <!-- /container -->