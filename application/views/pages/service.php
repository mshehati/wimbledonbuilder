<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
			<div class="rg-image"></div>
			<div class="rg-loading"></div>
			<div class="rg-caption-wrapper">
				<div class="rg-caption" style="display:none;">
					<p></p>
				</div>
			</div>
	</div>
</script>
<div class="container wrapper" role="main">
	<div class='row'>
		<div class='col-md-6 col-md-push-6'>
			<div id="rg-gallery" class="rg-gallery">
   				<div class="rg-thumbs">
       				<div class="es-carousel-wrapper">
           				<div class="es-nav">
               				<span class="es-nav-prev">Previous</span>
               				<span class="es-nav-next">Next</span>
           				</div>
           				<div class="es-carousel">
               				<ul>
               					<?php foreach($images as $image):?>
                   					<li>
                   						<a href="#">
                   							<img src="<?php echo base_url('uploads') .'/services/' . $image['filename'];?>" data-large="<?php echo base_url('uploads/services') .'/' . $image['filename'];?>" alt="" data-description="" />
                   						</a>
                           			</li>
                           		<?php endforeach; ?>
               				</ul>
           				</div>
       				</div>
   				</div>
   			</div><!-- rg-gallery -->
		</div>
		<div class="col-md-6 col-md-pull-6">
			<h2>
				<?php echo $service['title'];?>
			</h2>
			<hr/>
			<?php echo $service['full_desc'];?>
		</div>
	</div>
	<section class="service-listing">
		<div class="page-header">
			<h2>Other Services</h2>
		</div>
		<div class="row">
			<?php foreach($services as $service):?>
				<div class="col-md-4 col-sm-6">
	          		<div class="panel panel-default panel-custom">
	            		<div class="panel-heading">
	              			<h2 class="panel-title"><?php echo $service['title'];?></h2>
	            		</div>
	            		<div class="panel-body">
	              			<?php echo $service['short_desc'];?>
	            		</div>
	            		<div class="panel-footer">
	            			<a href="<?php echo site_url('services').'/s/'.urlencode($service['hash_title']);?> " class="btn btn-default btn-custom">Find out more</a>
	            		</div>
	          		</div>
	      		</div>
			<?php endforeach;?>
	   	</div>
	</section>
</div> <!-- /container -->
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.tmpl.min.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.easing.1.3.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/jquery.elastislide.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url('style') . '/js/gallery.js';?>"></script>