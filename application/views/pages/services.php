<div class="container wrapper" role="main">
	<section class="service-listing">
		<h2>Services</h2>
		<hr/>
		<div class="row">
			<?php foreach($services as $service):?>
				<div class="col-md-4 col-sm-6">
	          		<div class="panel panel-default panel-custom">
	            		<div class="panel-heading">
	              			<h2 class="panel-title"><?php echo $service['title'];?></h2>
	            		</div>
	            		<div class="panel-body">
	              			<?php echo $service['short_desc'];?>
	            		</div>
	            		<div class="panel-footer">
	            			<a href="<?php echo site_url('services').'/s/'.urlencode($service['hash_title']);?> " class="btn btn-default btn-custom">Find out more</a>
	            		</div>
	          		</div>
	      		</div>
			<?php endforeach;?>
      	</div>
	</section>
	<hr />
</div> <!-- /container -->