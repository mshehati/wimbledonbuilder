<div class="container wrapper" role="main">
	<div>
		<h2>Sitemap</h2>
	</div>
	<hr />
	<div class='sitemap'>
		<p class="lead">
			<a href="<?php echo site_url();?>"><span class="glyphicon glyphicon-chevron-right"></span> Home</a>
			<a href="<?php echo site_url('about');?>"><span class="glyphicon glyphicon-chevron-right"></span> About Us</a>
			<a href="<?php echo site_url('services');?>"><span class="glyphicon glyphicon-chevron-right"></span> Services</a>
		</p>
			<ul>
				<?php foreach($services as $service):?>
					<li><a href="<?php echo site_url('services/s').'/'.urlencode($service['hash_title']);?>"><span class="glyphicon glyphicon-menu-right"></span> <?php echo $service['title'];?></a></li>
				<?php endforeach;?>
			</ul>
		<p class="lead">
			<a href="<?php echo site_url('gallery');?>"><span class="glyphicon glyphicon-chevron-right"></span> Gallery</a>
		</p>
			<ul>
				<?php foreach($galleries as $g):?>
					<li><a href="<?php echo site_url('gallery/view').'/'.urlencode($g['hash_title']);?>"><span class="glyphicon glyphicon-menu-right"></span> <?php echo $g['title'];?></a></li>
				<?php endforeach;?>
			</ul>
		<p class="lead">
			<a href="<?php echo site_url('testimonials');?>"><span class="glyphicon glyphicon-chevron-right"></span> Testimonials</a>
		</p>
		<p class="lead">
			<a href="#" onclick="return false;"><span class="glyphicon glyphicon-chevron-right"></span> Articles</a>
		</p>
			<ul>
				<?php foreach($articles as $article):?>
					<li><a href="<?php echo site_url('articles/a').'/'.$article['id'];?>"><span class="glyphicon glyphicon-menu-right"></span> <?php echo $article['title'];?></a></li>
				<?php endforeach;?>
			</ul>
		<p>
			<a href="<?php echo site_url('contact');?>"><span class="glyphicon glyphicon-chevron-right"></span> Contact Us</a>
		</p>
	</div>
	<hr />
</div> <!-- /container -->