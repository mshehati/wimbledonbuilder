<div class="container wrapper" role="main">
	<div>
		<h2>This is what our clients are saying about us</h2>
	</div>		
    <hr />
    <div class="testimonials">
    	<?php foreach ($testimonials as $i => $t):?>
    		<div class="testimonial">
    			<span class="testimonial-head"><?php echo $t['people'];?></span>
    			<span class='testimonial-date'><?php echo $t['_date'];?> <?php echo $t['location']!== '' ? '| '.$t['location']: ''; ?></span> 
				<div class="testimonial-content"><?php echo $t['content'];?></div>
    		</div>
    	<?php endforeach;?>
    </div>
</div> <!-- /container -->